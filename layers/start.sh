#!/bin/sh
docker build . -t layer-test
docker run --rm layer-test
docker history layer-test >> history.txt
docker save layer-test > layer.tar